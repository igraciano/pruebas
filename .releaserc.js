const { execSync } = require('child_process');

module.exports = isDryRun() ? getDryRunConfig() : getCIConfig();

function getDryRunConfig() {
  return {
    repositoryUrl: getLocalRepoUrl(),
    branches: getCurrentBranch(),
    plugins: [
      '@semantic-release/commit-analyzer',
      './scripts/release-notes-generator-master',
    ],
  };
}

function getCIConfig() {
  // contains your normal semantic-release config
  // this will be used on your CI environment
  return {
    "branches": [
      {"name": "v1.x.x", "range": "1.x.x"}, 
      {"name": "main", "channel": "main"}, 
      {"name": "alpha", "channel": "alpha", "prerelease": "alpha"}, 
      {"name": "beta", "channel": "beta", "prerelease": "beta"} 
    ],
    "plugins": [
      [
        "@semantic-release/commit-analyzer", 
        {
          "releaseRules": [
            {"type": "fix", "release": "patch"},
            {"type": "new", "release": "patch"},
            {"type": "refactor", "release": "patch"},
            {"type": "announcement", "release": "minor"}
          ],
          "parserOpts": {
            "noteKeywords": ["BREAKING CHANGE", "BREAKING CHANGES"]
          }
        }
      ],
      "./scripts/release-notes-generator-master",
      [ "@semantic-release/changelog", {"changelogFile": "CHANGELOG.md"} ],
      [
        "@semantic-release/exec",
        {
          "verifyReleaseCmd": "echo ${nextRelease.version} > VERSION.txt"
        }
      ],
      [
        "@semantic-release/git",
        {
          "assets": ["package.json", "CHANGELOG.md"],
          "message": "chore(release): ${nextRelease.version} [skip ci]  \n\n${nextRelease.notes}"
        }
      ],
      [
        "@semantic-release/gitlab",
        {
          "gitlabUrl": "https://gitlab.com/playvox/teams/orion/changelog-chromatic-delete.git",
          "assets": [
            {
              "path": "CHANGELOG.md",
              "label": "changelog file"
            },
            {
              "path": "package.json",
              "label": "package file"
            }
          ]
        }
      ]
    ]
  };
}

function isDryRun() {
  return process.argv.includes('--dry-run');
}

function getLocalRepoUrl() {
  const topLevelDir = execSync('git rev-parse --show-toplevel')
    .toString()
    .trim();

  return `file://${topLevelDir}/.git`;
}

function getCurrentBranch() {
  return execSync('git rev-parse --abbrev-ref HEAD')
    .toString()
    .trim();
}
