# 1.0.0 [storybook](https://10d7469f--6137a5aaff04f6003a284c75.chromatic.com/?path=/story/about--page) (2022-09-09)


### Bug Fixes

* 123 ([0ceb4f4](https://gitlab.com/igraciano/pruebas/commit/0ceb4f494907c0e89ef832687967abdf94d42856))
* 33 ([b2626ec](https://gitlab.com/igraciano/pruebas/commit/b2626ec34ea9463c14c80594a192726243ee3791))
* 35 ([10d7469](https://gitlab.com/igraciano/pruebas/commit/10d7469f8fb797169ee39f53156aa4f3f084a82e))
* add yml ([49b3035](https://gitlab.com/igraciano/pruebas/commit/49b30358f8bb1e830748823068038508c5311b2c))
* prueba 1 ([c040213](https://gitlab.com/igraciano/pruebas/commit/c040213742cb359de1f9f191b8f81144d35404ea))
* prueba 4 ([3c47c29](https://gitlab.com/igraciano/pruebas/commit/3c47c29d86dfcd070c4cc4f6182db6519b5fe5f7))
* prueba2 ([f4979e7](https://gitlab.com/igraciano/pruebas/commit/f4979e766b8b7c1ec6357355cdff9fd6b9054355))
* remove slack hook ([9c1096e](https://gitlab.com/igraciano/pruebas/commit/9c1096eed4175b07671a9374a5607383b43536a4))
